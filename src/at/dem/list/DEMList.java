package at.dem.list;

public class DEMList implements HTLList{

        private Node root;

        @Override
        public void add(int value) {
            Node n = new Node(value);
            if (root == null) {
                root = n;
            } else {
                getLastNode().next = n;
            }
        }

        private Node getLastNode() {
            Node n = root;
            while (n.getNext() != null) {
                n = n.next;
            }
            return n;
        }

        @Override
        public int get(int index) {
            Node n = root;
            int i = 0;
            while (i<index){
                if (n.next==null){
                    throw new IndexOutOfBoundsException("Keine Daten vorhanden an dieser Stelle");
                }
                n = n.next;
                i++;
            }
            if(root==null){
                throw new IndexOutOfBoundsException("root wurde gelöscht");
            }
            return n.getValue();
        }

        @Override
        public void remove(int index) {
            Node n = root;
            int i = 0;
            Node help = null;
            Node help2 = null;
            for(int j=0;j<index;j++){

                if(n.next == null){
                    help = help2;
                }
                if(j == (index - 1) && n.next != null){
                    help = n.next;
                }
                help2 = n.next;
                n = n.next;
            }
            n = root;
            while (i<index) {
                  if(i == (index-2)){
                    n.setNext(help);
                  }
                  if(index == 1){
                      root = n.next;
                  }
                n = n.next;
                i++;
            }
            return;
        }

        @Override
        public void clear() {

            root = null;

            return;
        }


}
