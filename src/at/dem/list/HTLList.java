package at.dem.list;

public interface HTLList {
    public void add(int value);
    public int get(int index);
    public void remove(int index);
    public void clear();
}
