package at.dem.queue;

public interface HTLQueue {
    public void enqueue(int value);
    public int dequeue();

}
