package at.dem.queue;

public class Node {
    private int value;

    public at.dem.queue.Node next;
    public at.dem.queue.Node before;

    public Node(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public at.dem.queue.Node getNext() {
        return next;
    }

    public void setNext(at.dem.queue.Node next) {
        this.next = next;
    }
    public at.dem.queue.Node getBefore() {
        return before;
    }

    public void setBefore(at.dem.queue.Node before) {
        this.before = before;
    }
}
