package at.dem.stack;

import at.dem.stack.exceptions.StackEmptyException;

public interface HTLStack {
    public void push(int value);
    public int pop() throws StackEmptyException;

}
