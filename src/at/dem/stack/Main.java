package at.dem.stack;

import at.dem.stack.exceptions.StackEmptyException;

public class Main {
    public  static  void  main(String[] args){
        DEMstack stack = new DEMstack();
        stack.push(12);
        stack.push(33);
        stack.push(15);
        try {
            System.out.println(stack.pop());
            System.out.println(stack.pop());
        } catch (StackEmptyException e) {
            System.out.println(e.getMessage());
        }

    }
}
