package at.dem.stack;

public class Node {
    private int value;

    public at.dem.stack.Node next;

    public Node(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public at.dem.stack.Node getNext() {
        return next;
    }

    public void setNext(at.dem.stack.Node next) {
        this.next = next;
    }
}
