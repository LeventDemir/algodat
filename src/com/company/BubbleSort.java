package com.company;

public class BubbleSort implements Sorter{

    @Override
    public int[] sort(int[] arr) {
        int count = arr.length -1;
        while (count > 0) {
        for(int i=0;i<arr.length -1;i++){
                if (arr[i] > arr[i + 1]) {
                    int arr1 = arr[i];
                    int arr2 = arr[i];
                    arr[i] = arr2;
                    arr[i + 1] = arr1;
                }
            }
            count--;
        }
        return arr;
    }
}
