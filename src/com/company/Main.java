package com.company;

public class Main {

    public static void main(String[] args) {
        int [] arr = SortHelper.generateArray(10);
        // Sorter sorter = new BubbleSort();
       // Sorter sorter = new InsertSort();
       // Sorter sorter = new SelectionSort();
        Sorter sorter = new BucketSort();
        printArray(arr);
        int[] sorted = sorter.sort(arr);
        printArray(sorted);

       //Sorter selectionsorter = new SelectionSort();
       //selectionsorter.sort(arr);

    }
    private  static void  printArray(int[] arr){
        for (int value: arr){
            System.out.print(value + " - ");
        }
        System.out.println();
    }

}
