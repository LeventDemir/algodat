package com.company;

public class SelectionSort implements Sorter{
    @Override
    public int[] sort(int[] arr) {

        int counter = arr.length -1;
        while(counter > 0) {

            for(int j=0;j<arr.length - 1;j++) {
                for (int i = j; i < arr.length; i++) {
                    if (arr[i] <= arr[j]) {
                        int var1 = arr[i];
                        int var2 = arr[j];
                        arr[i] = var2;
                        arr[j] = var1;

                    }
                }
            }

            counter--;
        }
            return arr;
    }
}
