package com.company;

public interface Sorter {
    public int[] sort(int[] arr);
}
